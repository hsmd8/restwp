<?php
/**
 * ReduxFramework Sample Config File
 * For full documentation, please visit: http://docs.reduxframework.com/
 */

if (!class_exists('Redux')) {
    $rest_options = array();
    return;
}


// This is your option name where all the Redux data is stored.
$opt_name = "rest_options";

// This line is only for altering the demo. Can be easily removed.
//$opt_name = apply_filters( 'redux_demo/opt_name', $opt_name );

/*
 *
 * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
 *
 */

$sampleHTML = '';
if (file_exists(dirname(__FILE__) . '/info-html.html')) {
    Redux_Functions::initWpFilesystem();

    global $wp_filesystem;

    $sampleHTML = $wp_filesystem->get_contents(dirname(__FILE__) . '/info-html.html');
}


$revolutionslider = array();
$revolutionslider[0] = 'None';

if (class_exists('RevSlider')) {
    $slider = new RevSlider();
    $arrSliders = $slider->getArrSliders();
    foreach ($arrSliders as $revSlider) {
        $revolutionslider[$revSlider->getAlias()] = $revSlider->getTitle();
    }
}

/**
 * ---> SET ARGUMENTS
 * All the possible arguments for Redux.
 * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
 * */

$theme = wp_get_theme(); // For use with some settings. Not necessary.

$args = array(
    // TYPICAL -> Change these values as you need/desire
    'opt_name' => $opt_name,
    // This is where your data is stored in the database and also becomes your global variable name.
    'display_name' => $theme->get('Name'),
    // Name that appears at the top of your panel
    'display_version' => $theme->get('Version'),
    // Version that appears at the top of your panel
    'menu_type' => 'menu',
    //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
    'allow_sub_menu' => true,
    // Show the sections below the admin menu item or not
    'menu_title' => 'Rest Hostel',
    'page_title' => 'Rest Hostel',
    // You will need to generate a Google API key to use this feature.
    // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
    'google_api_key' => '',
    // Set it you want google fonts to update weekly. A google_api_key value is required.
    'google_update_weekly' => false,
    // Must be defined to add google fonts to the typography module
    'async_typography' => true,
    // Use a asynchronous font on the front end or font string
    //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
    'admin_bar' => true,
    // Show the panel pages on the admin bar
    'admin_bar_icon' => 'dashicons-portfolio',
    // Choose an icon for the admin bar menu
    'admin_bar_priority' => 50,
    // Choose an priority for the admin bar menu
    'global_variable' => '',
    // Set a different name for your global variable other than the opt_name
    'dev_mode' => false,
    // Show the time the page took to load, etc
    'update_notice' => true,
    // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
    'customizer' => true,
    // Enable basic customizer support
    //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
    //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

    // OPTIONAL -> Give you extra features
    'page_priority' => null,
    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
    'page_parent' => 'themes.php',
    // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
    'page_permissions' => 'manage_options',
    // Permissions needed to access the options panel.
    'menu_icon' => '',
    // Specify a custom URL to an icon
    'last_tab' => '',
    // Force your panel to always open to a specific tab (by id)
    'page_icon' => 'icon-themes',
    // Icon displayed in the admin panel next to your menu_title
    'page_slug' => '',
    // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
    'save_defaults' => true,
    // On load save the defaults to DB before user clicks save or not
    'default_show' => false,
    // If true, shows the default value next to each field that is not the default value.
    'default_mark' => '',
    // What to print by the field's title if the value shown is default. Suggested: *
    'show_import_export' => true,
    // Shows the Import/Export panel when not used as a field.

    // CAREFUL -> These options are for advanced use only
    'transient_time' => 60 * MINUTE_IN_SECONDS,
    'output' => true,
    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
    'output_tag' => true,
    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
    // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

    // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
    'database' => '',
    // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
    'use_cdn' => true,
    // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

    // HINTS
    'hints' => array(
        'icon' => 'el el-question-sign',
        'icon_position' => 'right',
        'icon_color' => 'lightgray',
        'icon_size' => 'normal',
        'tip_style' => array(
            'color' => 'red',
            'shadow' => true,
            'rounded' => false,
            'style' => '',
        ),
        'tip_position' => array(
            'my' => 'top left',
            'at' => 'bottom right',
        ),
        'tip_effect' => array(
            'show' => array(
                'effect' => 'slide',
                'duration' => '500',
                'event' => 'mouseover',
            ),
            'hide' => array(
                'effect' => 'slide',
                'duration' => '500',
                'event' => 'click mouseleave',
            ),
        ),
    )
);


// Panel Intro text -> before the form
if (!isset($args['global_variable']) || $args['global_variable'] !== false) {
    if (!empty($args['global_variable'])) {
        $v = $args['global_variable'];
    }
}

// Add content after the form.
$args['footer_text'] = '';

Redux::setArgs($opt_name, $args);

/*
 * ---> END ARGUMENTS
 */




// -> START Basic Settings Selection
Redux::setSection($opt_name, array(
    'title' => 'Ustawienia podstawowe',
    'id' => 'rest',
    'desc' => 'Podstawowa konfiguracja skórki',
    'icon' => 'el el-home',
    'fields' => array(
        array(
            'id' => 'primary-accent-color',
            'type' => 'color',
            'transparent' => false,
            'validate' => 'color',
            // 'compiler'  => true,
            'title' => 'Motyw przewodni',
            'desc' => 'Wybierz kolor motywu przewodniego',
            'default' => '#d1a57b',
        ),

        array(
            'id' => 'background-color',
            'type' => 'color',
            'transparent' => false,
            'title' => 'Kolor tła strony',
            'desc' => 'Ogólny kolor tła, przydatne dla strony 404 itp. Kolor tła podstron można (ale nie trzeba) ustawiać indywidualnie per strona',
            'default' => '#ffffff',
            'validate' => 'color',
        ),


        array(
            'id' => 'error-page-404',
            'type' => 'color',
            'transparent' => false,
            'title' => 'Kolor cienia na stronie error 404',
            'desc' => 'Cień 404 pod napisami - wybierz bardzo jasny kolor',
            'default' => '#f7f7f7',
            'validate' => 'color',
        ),

        array(
            'id' => 'preloader-enabled',
            'type' => 'switch',
            'title' => 'Włącz preloader',
            'subtitle' => 'Click <code>Wł</code> żeby włączyć.',
            'default' => false
        ),

        array(
            'id' => 'preloader-logo',
            'type' => 'media',
            'desc' => 'Wybierz logo do preloadera',
            'title' => 'Preloader logo',
            'required' => array('preloader-enabled', '=', true),
        ),

        array(
            'id' => 'preloader-bg',
            'type' => 'color',
            'validate' => 'color',
            // 'compiler'  => true,
            'title' => 'Kolor tła preloadera',
            'default' => '#ffffff',
            'required' => array('preloader-enabled', '=', true),
            'transparent' => false,
        ),

        array(
            'id' => 'preloader-font',
            'type' => 'color',
            'validate' => 'color',
            // 'compiler'  => true,
            'title' => 'Kolor czcionki preloadera',
            'default' => '#ffffff',
            'required' => array('preloader-enabled', '=', true),
            'transparent' => false,
        ),

        array(
            'id' => 'preloader-pages',
            'type' => 'select',
            'title' => 'Włącz preloader na:',
            'desc' => 'Zdefiniuj na których stronach chcesz używać preloadera',
            //Must provide key => value pairs for select options
            'options' => array(
                'all' => 'Wszystich stronach',
                'main' => 'Tylko na stronie głównej',
            ),
            'default' => 'all',
            'required' => array('preloader-enabled', '=', true)
        ),

        array(
            'id' => 'slider-enabled',
            'type' => 'switch',
            'title' => 'Słącz slider na stronie głównej',
            'subtitle' => 'Kliknij <code>Wł</code> żeby aktywować Revolution Slider na stronie głównej',
            'default' => false
        ),

        array(
            'id' => 'slider',
            'type' => 'select',
            'title' => 'Wybierz slider',
            'options' => $revolutionslider,
            'default' => $revolutionslider[0],
            'select2' => array('allowClear' => false),
            'required' => array('slider-enabled', '=', true)
        ),

    ),
));

// -> START Navigation Selection

Redux::setSection($opt_name, array(
    'title' => 'Navigacja',
    'id' => 'navigation',
    'subsection' => false,
    'desc' => 'Ustawienia dotyczące paska navigacji, menu uraz paska górnego',
    'fields' => array(

        array(
            'id'        => 'navbar-background',
            'type'      => 'color_rgba',
            'title'     => 'Tło paska navigacji',

            'default'   => array(
                'color'     => '#ffffff',
                'alpha'     => 0.5
            ),
            // These options display a fully functional color palette.  Omit this argument
            // for the minimal color picker, and change as desired.
            'options'       => array(
                'show_input'                => true,
                'show_initial'              => true,
                'show_alpha'                => true,
                'show_palette'              => true,
                'show_palette_only'         => false,
                'show_selection_palette'    => true,
                'max_palette_size'          => 10,
                'allow_empty'               => true,
                'clickout_fires_change'     => false,
                'choose_text'               => 'Wybierz',
                'cancel_text'               => 'Anuluj',
                'show_buttons'              => true,
                'use_extended_classes'      => true,
                'palette'                   => null,  // show default
                'input_text'                => 'Wybierz kolor'
            ),
        ),


        array(
            'id' => 'mobile-logo',
            'type' => 'media',
            'desc' => 'Wybierz logo do wersji mobilnej',
            'title' => 'Logo na mobile',
        ),

        array(
            'id' => 'mobile-sticker',
            'type' => 'media',
            'desc' => 'Wybierz sticker do wersji mobilnej',
            'title' => 'Sticker na mobile',
        ),

        array(
            'id' => 'mobile-sticker-text',
            'type' => 'editor',
            'title' => 'Text na sticker - mobile',
        ),

        array(
            'id' => 'mobile-map-link',
            'type' => 'text',
            'title' => 'Link do mapy - mobile',
        ),

        array(
            'id' => 'mobile-map-text',
            'type' => 'text',
            'desc' => 'Text do wyświetlenia w linku do mapy - mobile',
            'title' => 'Labelka',
        ),


        array(
            'id' => 'rest-menu-font',
            'type' => 'typography',
            'title' => 'Czcionka menu',
            'subtitle' => 'Wybierz ustawienia dla czcionki w menu',
            'google' => true,
            // 'compiler'  => true,
            'all_styles' => true,
            'line-height' => false,
            'text-align' => false,
            'default' => array(
                'google' => true,
                'color' => '#202020',
                'font-family' => 'Playfair Display',
                'font-size' => '20px',
                'font-weight' => 400
            ),
        ),

        array(
            'id' => 'language-settings',
            'type' => 'color',
            'validate' => 'color',
            'transparent' => false,
            'title' => 'Kolor czcionki dla ustawień języka',
            'default' => '#ffffff',
        ),


        array(
            'id' => 'social-settings',
            'type' => 'color',
            'validate' => 'color',
            'transparent' => false,
            'title' => 'Kolor czcionki dla ikonek w górnym pasku',
            'default' => '#ffffff',
        ),

        array(
            'id' => 'hamburger-menu-color',
            'type' => 'color',
            'validate' => 'color',
            'transparent' => false,
            'title' => 'Kolor hamburgera :)',
            'default' => '#cbcbcb',
        ),




        array(
            'id' => 'favicon',
            'type' => 'media',
            'title' => 'Favicon',
            'subtitle' => 'Wybierz favikonkę',
        ),


        array(
            'id' => 'social-header',
            'type' => 'select',
            'multi' => true,
            'title' => 'Ikonki w górnym pasku',
            'desc' => 'Wybierz ikonki social mediów które chcesz wyświetlić w górnym pasku. Linki ustawisz w sekcji SOCIAL MEDIA',
            //Must provide key => value pairs for radio options
            'options' => array(
                'fa-behance' => 'Behance',
                'fa-facebook' => 'Facebook',
                'fa-tripadvisor' => 'Trip Avisor',
                'fa-flickr' => 'Flickr',
                'fa-google-plus' => 'Google+',
                'fa-instagram' => 'Instagram',
                'fa-pinterest' => 'Pinterest',
                'fa-skype' => 'Skype',
                'fa-stumbleupon' => 'Stumbleupon',
                'fa-tumblr' => 'Tumblr',
                'fa-twitter' => 'Twitter',
                'fa-vimeo' => 'Vimeo',
                'fa-youtube' => 'Youtube',
            ),
            //'required' => array( 'opt-select', 'equals', array( '1', '3' ) ),
            'default' => array('fa-facebook', 'fa-twitter')
        ),


        array(
            'id' => 'show_email',
            'title' => 'Wyswietl adres email',
            'type' => 'switch',
            'default' => true
        ),

        array(
            'id' => 'email',
            'type' => 'text',
            'required' => array('show_email', '=', true),
            'title' => 'Adres email',
            'default' => 'rezerwacja@hostelrest.pl',
        ),

        array(
            'id' => 'show_phone',
            'type' => 'switch',
            'title' => 'Wyswietl numer telefonu',
            'default' => true,
        ),

        array(
            'id' => 'phone',
            'type' => 'text',
            'required' => array('show_phone', '=', true),
            'title' => 'Numer telefonu',
            'default' => '(+48 12) 425 96 00',
        ),


        array(
            'id' => 'show_info',
            'type' => 'switch',
            'title' => 'Wyswietl info o recepcji',
            'default' => true,
        ),

        array(
            'id' => 'recepcja',
            'type' => 'text',
            'required' => array('show_info', '=', true),
            'title' => 'Info o recepcji',
            'desc' => 'Trzymaj się podanego formatu ([:kod języka] tłumaczenie)',
            'default' => '[:en]Recepcja czynna 24h [:pl]Recepcja czynna 24h [:de]Recepcja czynna 24h [:ru] Recepcja czynna 24h',
        ),

    )
));


// -> START Typography Selection
Redux::setSection($opt_name, array(
    'title' => 'Typografia',
    'id' => 'typography',
    'desc' => 'Ustawienia czcionek',
    'icon' => 'el el-font',
    'fields' => array(

        array(
            'id' => 'rest-typography-body',
            'type' => 'typography',
            'title' => 'Czcionka główna',
            'google' => true,
            'text-align' => false,
            // 'compiler'  => true,
            'default' => array(
                'color' => '#000000',
                'font-size' => '17px',
                'font-family' => 'Lato',
                'font-weight' => 300,
                'line-height' => '30px',
            ),
        ),


        array(
            'id' => 'rest-typography-heading',
            'type' => 'typography',
            'title' => 'Nagłówki - różne',
            'google' => true,
            // 'compiler'  => true,
            'all_styles' => true,
            'font-weight' => false,
            'line-height' => false,
            'font-size' => false,
            'text-align' => false,
            'default' => array(
                'google' => true,
                'color' => '#000000',
                'font-family' => 'Playfair Display',
            ),
        ),

        array(
            'id' => 'rest-typography-h1',
            'type' => 'typography',
            'title' => 'Nagłówni z tagiem H1',
            'google' => true,
            // 'compiler'  => true,
            'all_styles' => true,
            'line-height' => false,
            'text-align' => false,
            'default' => array(
                'google' => true,
                'color' => '#000000',
                'font-family' => 'Playfair Display',
                'font-weight' => 300,
                'font-size' => '36px',
            ),
        ),


        array(
            'id' => 'rest-typography-h2',
            'type' => 'typography',
            'title' => 'Nagłówni z tagiem H2',
            'google' => true,
            // 'compiler'  => true,
            'all_styles' => true,
            'line-height' => false,
            'text-align' => false,
            'default' => array(
                'google' => true,
                'color' => '#000000',
                'font-family' => 'Playfair Display',
                'font-weight' => 300,
                'font-size' => '32px',
            ),
        ),

        array(
            'id' => 'rest-typography-h3',
            'type' => 'typography',
            'title' => 'Nagłówni z tagiem H3',
            'google' => true,
            // 'compiler'  => true,
            'all_styles' => true,
            'line-height' => false,
            'text-align' => false,
            'default' => array(
                'color' => '#000000',
                'font-family' => 'Playfair Display',
                'font-weight' => 300,
                'font-size' => '28px',
            ),
        ),


        array(
            'id' => 'rest-typography-h4',
            'type' => 'typography',
            'title' => 'Nagłówni z tagiem H4',
            'google' => true,
            // 'compiler'  => true,
            'all_styles' => true,
            'line-height' => false,
            'text-align' => false,
            'default' => array(
                'color' => '#000000',
                'font-family' => 'Playfair Display',
                'font-weight' => 300,
                'font-size' => '24px',
            ),
        ),

        array(
            'id' => 'rest-typography-h5',
            'type' => 'typography',
            'title' => 'Nagłówni z tagiem H5',
            'google' => true,
            // 'compiler'  => true,
            'all_styles' => true,
            'line-height' => false,
            'text-align' => false,
            'default' => array(
                'color' => '#000000',
                'font-family' => 'Playfair Display',
                'font-weight' => 300,
                'font-size' => '20px',
            ),
        ),

        array(
            'id' => 'rest-typography-h6',
            'type' => 'typography',
            'title' => 'Nagłówni z tagiem H6',
            'google' => true,
            // 'compiler'  => true,
            'all_styles' => true,
            'line-height' => false,
            'text-align' => false,
            'default' => array(
                'color' => '#000000',
                'font-family' => 'Playfair Display',
                'font-weight' => 300,
                'font-size' => '18px',
            ),
        ),


    )
));


// -> START Social Profiles Selection
Redux::setSection($opt_name, array(
    'title' => 'Solcial Media',
    'id' => 'social',
    'desc' => 'Ustaw linki do swoich profili w social mediach - używane w headerze i footerze',
    'icon' => 'el el-thumbs-up',
    'fields' => array(

        array(
            'id' => 'fa-behance',
            'type' => 'text',
            'default' => '#',
            'title' => 'Behance Url'
        ),
        array(
            'id' => 'fa-facebook',
            'type' => 'text',
            'default' => '#',
            'title' => 'Facebook Url'
        ),

        array(
            'id' => 'fa-tripadvisor',
            'type' => 'text',
            'default' => '#',
            'title' => 'Trip Advisor Url'
        ),

        array(
            'id' => 'fa-flickr',
            'type' => 'text',
            'default' => '#',
            'title' => 'Flickr Url'
        ),
        array(
            'id' => 'fa-google-plus',
            'type' => 'text',
            'default' => '#',
            'title' => 'Google+ Url'
        ),
        array(
            'id' => 'fa-instagram',
            'type' => 'text',
            'default' => '#',
            'title' => 'Instagram Url'
        ),
        array(
            'id' => 'fa-pinterest',
            'type' => 'text',
            'default' => '#',
            'title' => 'Pinterest Url'
        ),
        array(
            'id' => 'fa-skype',
            'type' => 'text',
            'default' => '#',
            'title' => 'Skype Url'
        ),
        array(
            'id' => 'fa-stumbleupon',
            'type' => 'text',
            'default' => '#',
            'title' => 'Stumbleupon Url'
        ),
        array(
            'id' => 'fa-tumblr',
            'type' => 'text',
            'default' => '#',
            'title' => 'Tumblr Url'
        ),
        array(
            'id' => 'fa-twitter',
            'type' => 'text',
            'default' => '#',
            'title' => 'Twitter Url'
        ),
        array(
            'id' => 'fa-vimeo',
            'type' => 'text',
            'default' => '#',
            'title' => 'Vimeo Url'
        ),
        array(
            'id' => 'fa-youtube',
            'type' => 'text',
            'default' => '#',
            'title' => 'Youtube Url'
        ),


    )

));

// -> START Footer Selection
Redux::setSection($opt_name, array(
    'title' => 'Stopka',
    'id' => 'footers',
    'icon' => 'el el-list-alt',
    'fields' => array(




        array(
            'id' => 'footer-logo',
            'type' => 'media',
            'title' => 'Logo w stopce',
        ),


        array(
            'id' => 'footer-bg-color',
            'type' => 'color',
            'transparent' => false,
            'validate' => 'color',
            'title' => 'Kolor tła stopki',
            'default' => '#3a3733',
        ),

        array(
            'id' => 'lower-footer-bg-color',
            'type' => 'color',
            'transparent' => false,
            'validate' => 'color',
            'title' => 'Kolor tła stopki - copyright',
            'default' => '#1e1c19',
        ),

        array(
            'id' => 'footer-text-color',
            'type' => 'color',
            'transparent' => false,
            'validate' => 'color',
            'title' => 'Kolor tekstu stopki',
            'default' => '#8d8d8d',
        ),

        array(
            'id' => 'footer-icon-color',
            'type' => 'color',
            'validate' => 'color',
            'transparent' => false,
            'title' => 'Kolor ikonek i linków w stopce',
            'default' => '#d1a57b',
        ),


        array(
            'id' => 'social_footer',
            'type' => 'select',
            'multi' => true,
            'title' => 'Ikonki w stopce',
            'desc' => 'Wybierz ikonki social mediów które chcesz wyświetlić w stopce. Linki ustawisz w sekcji SOCIAL MEDIA',
            //Must provide key => value pairs for radio options
            'options' => array(
                'fa-behance' => 'Behance',
                'fa-facebook' => 'Facebook',
                'fa-tripadvisor' => 'Trip Avisor',
                'fa-flickr' => 'Flickr',
                'fa-google-plus' => 'Google+',
                'fa-instagram' => 'Instagram',
                'fa-pinterest' => 'Pinterest',
                'fa-skype' => 'Skype',
                'fa-stumbleupon' => 'Stumbleupon',
                'fa-tumblr' => 'Tumblr',
                'fa-twitter' => 'Twitter',
                'fa-vimeo' => 'Vimeo',
                'fa-youtube' => 'Youtube',
            ),
            //'required' => array( 'opt-select', 'equals', array( '1', '3' ) ),
            'default' => array('fa-facebook', 'fa-twitter')
        ),


        array(
            'id' => 'footer_adres',
            'type' => 'text',
            'title' => 'Adres w footerze',
            'default' => 'ul. Sołtysowska 37a Kraków, 31-589',
        ),
        array(
            'id' => 'footer_telefon_link',
            'type' => 'text',
            'title' =>'Link do telefonu : dzwoni na mobile - zachowaj zadany format',
            'default' => 'tel:+48124259600',
        ),

        array(
            'id' => 'footer_telefon',
            'type' => 'text',
            'title' => 'Telefon w footerze - wyświetlany',
            'default' => '(+48 12) 425 96 00 ',
        ),

        array(
            'id' => 'lower_footer',
            'type' => 'text',
            'title' => 'Copyright tekst',
            'default' => '<i class="fa fa-copyright"></i> 2017 <a href="http://www.amgtrading.pl/">AMG TRADING Sp. z o.o.</a> 02-232 Warszawa ul. Łopuszańska 53',
        ),



    )


));




// -> START Social Profiles Selection
Redux::setSection($opt_name, array(
    'title' => 'Mapa Google',
    'id' => 'map',
    'desc' => 'Ustawienia mapy google',
    'icon' => 'el el-thumbs-up',
    'fields' => array(

        array(
            'id' => 'map-api',
            'type' => 'text',
            'default' => 'AIzaSyAxDSXmGtzXm79Mm8crrtWnAauJaGNiTR4',
            'title' => 'API Key'
        ),

        array(
            'id' => 'map-zoom',
            'type' => 'text',
            'default' => '11',
            'title' => 'Zoom'
        ),

        array(
            'id' => 'map-marker',
            'type' => 'media',
            'desc' => 'Wybierz znacznik markera',
            'title' => 'Marker'
        ),

        array(
            'id' => 'map-lat',
            'type' => 'text',
            'default' => '50.062722',
            'title' => 'Latitude'
        ),

        array(
            'id' => 'map-lng',
            'type' => 'text',
            'default' => '20.022764',
            'title' => 'Longitude'
        ),

        array(
            'id' => 'map-height',
            'type' => 'text',
            'default' => '400',
            'title' => 'Wysokość w pixelach'
        ),

        array(
            'id' => 'map-title',
            'type' => 'text',
            'default' => 'Rest Hostel',
            'title' => 'Tytuł'
        ),

        array(
            'id' => 'map-popup',
            'type' => 'textarea',
            'default' => '<b>Rest Hostel</b><p>
Sołtysowska 37a <br>31-589 Kraków <br>
</p>
<a class="test" href="https://maps.google.com/maps?ll=50.062722,20.022764&z=16&t=m&hl=pl-PL&gl=PL&mapclient=embed&daddr=Hostel%20REST%20So%C5%82tysowska%2037a%2031-589%20Krak%C3%B3w@50.06272209999999,20.0227642">Jak dojechać</a>',
            'title' => 'Opis'
        ),


    )

));


// -> START Custom Code Selection
Redux::setSection($opt_name, array(
    'title' => 'Zaawansowane',
    'id' => 'editor',
    'customizer_width' => '500px',
    'icon' => 'el el-edit',
));
Redux::setSection($opt_name, array(
    'title' => 'Custom CSS',
    'id' => 'editor-css',
    //'icon'  => 'el el-home'
    'desc' => 'Tutaj możesz wstawić dodatkowy CSS',
    'subsection' => true,
    'fields' => array(

        array(
            'id' => 'include-css',
            'type' => 'switch',
            'title' => 'Załącz CSS',
            'subtitle' => 'Klinkij <code>wł</code> żeby załączyć dodatkowy CSS.',
            'default' => false
        ),


        array(
            'id' => 'custom-css',
            'type' => 'ace_editor',
            'title' => 'Dodatkowy CSS',
            'subtitle' => 'Wklej swój CSS',
            'mode' => 'css',
            'theme' => 'monokai',
            'default' => "#placeholder{\n   margin: 0 auto;\n}"
        ),


    )
));
Redux::setSection($opt_name, array(
    'title' => 'Custom Javascript',
    'id' => 'editor-js',
    'desc' => 'Tutaj możesz wstawić dodatkowy Javascript',
    //'icon'  => 'el el-home'
    'subsection' => true,
    'fields' => array(

        array(
            'id' => 'include-js',
            'type' => 'switch',
            'title' => 'Załącz Javascript',
            'subtitle' => 'Klinkij <code>wł</code> żeby załączyć dodatkowy Javascript.',
            'default' => false
        ),

        array(
            'id' => 'custom-js',
            'type' => 'ace_editor',
            'title' => 'Dodatkowy Javascript',
            'subtitle' => 'Wklej dodatkowy Javascript. Będzie umieszczony z dołu strony. Postaraj się nic nie zepsuć',
            'mode' => 'javascript',
            'default' => "jQuery(document).ready(function(){\n\nconsole.log('Hello!');\n\n});"
        ),


    )
));


if (file_exists(dirname(__FILE__) . '/../README.md')) {
    $section = array(
        'icon' => 'el el-list-alt',
        'title' => 'Dokumentacja Redux Framework',
        'fields' => array(
            array(
                'id' => '17',
                'type' => 'raw',
                'markdown' => true,
                'content_path' => dirname(__FILE__) . '/../README.md', // FULL PATH, not relative please
                //'content' => 'Raw content here',
            ),
        ),
    );
    Redux::setSection($opt_name, $section);
}
/*
 * <--- END SECTIONS
 */


/*
 *
 * YOU MUST PREFIX THE FUNCTIONS BELOW AND ACTION FUNCTION CALLS OR ANY OTHER CONFIG MAY OVERRIDE YOUR CODE.
 *
 */

/*
*
* --> Action hook examples
*
*/

// If Redux is running as a plugin, this will remove the demo notice and links
//add_action( 'redux/loaded', 'remove_demo' );

// Function to test the compiler hook and demo CSS output.
// Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);

// Change the arguments after they've been declared, but before the panel is created
//add_filter('redux/options/' . $opt_name . '/args', 'change_arguments' );

// Change the default value of a field after it's been set, but before it's been useds
//add_filter('redux/options/' . $opt_name . '/defaults', 'change_defaults' );

// Dynamically add a section. Can be also used to modify sections/fields
//add_filter('redux/options/' . $opt_name . '/sections', 'dynamic_section');

/**
 * This is a test function that will let you see when the compiler hook occurs.
 * It only runs if a field    set with compiler=>true is changed.
 * */

if (!function_exists('compiler_action')) {
    function compiler_action($options, $css, $changed_values)
    {
    }
}

/**
 * Custom function for the callback validation referenced above
 * */
if (!function_exists('redux_validate_callback_function')) {
    function redux_validate_callback_function($field, $value, $existing_value)
    {
        $error = false;
        $warning = false;

        //do your validation
        if ($value == 1) {
            $error = true;
            $value = $existing_value;
        } elseif ($value == 2) {
            $warning = true;
            $value = $existing_value;
        }

        $return['value'] = $value;

        if ($error == true) {
            $field['msg'] = 'your custom error message';
            $return['error'] = $field;
        }

        if ($warning == true) {
            $field['msg'] = 'your custom warning message';
            $return['warning'] = $field;
        }

        return $return;
    }
}

/**
 * Custom function for the callback referenced above
 */
if (!function_exists('redux_my_custom_field')) {
    function redux_my_custom_field($field, $value)
    {
        print_r($field);
        echo '<br/>';
        print_r($value);
    }
}

/**
 * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
 * Simply include this function in the child themes functions.php file.
 * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
 * so you must use get_template_directory_uri() if you want to use any of the built in icons
 * */
if (!function_exists('dynamic_section')) {
    function dynamic_section($sections)
    {
        //$sections = array();
        $sections[] = array(
            'title' => 'Section via hook',
            'desc' => 'This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.',
            'icon' => 'el el-paper-clip',
            // Leave this as a blank section, no options just some intro text set above.
            'fields' => array()
        );

        return $sections;
    }
}

/**
 * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
 * */
if (!function_exists('change_arguments')) {
    function change_arguments($args)
    {
        //$args['dev_mode'] = true;

        return $args;
    }
}

/**
 * Filter hook for filtering the default value of any given field. Very useful in development mode.
 * */
if (!function_exists('change_defaults')) {
    function change_defaults($defaults)
    {
        $defaults['str_replace'] = 'Testing filter hook!';

        return $defaults;
    }
}

/**
 * Removes the demo link and the notice of integrated demo from the redux-framework plugin
 */
if (!function_exists('remove_demo')) {
    function remove_demo()
    {
        // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
        if (class_exists('ReduxFrameworkPlugin')) {
            remove_filter('plugin_row_meta', array(
                ReduxFrameworkPlugin::instance(),
                'plugin_metalinks'
            ), null, 2);

            // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
            remove_action('admin_notices', array(ReduxFrameworkPlugin::instance(), 'admin_notices'));
        }
    }
}

add_action('redux/options/rest_options/saved', 'compile_less');

function compile_less()
{
    if (class_exists('Rest')) {

        Rest::generateCSS();

    }
}