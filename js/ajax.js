/**
 * Ajax loading for portfolio
 * @param $
 * @param galleryContainer
 * @param imageLoad
 * @constructor
 */
function AjaxGallery($, galleryContainer, imageLoad, imageCarousel, galleryItem) {

  var ajax = {

    // load image from slider to image container
    loadGalleryItem: function (link) {

      var rel,
        id,
        image,
        large;

      image = new Image();
      image.src = link.attr("href");

      rel = link.attr("data-rel");
      large = link.attr("data-link");
      id = 'currentGalleryImage';

      var imageLink = '<a href="'+large+'" id="'+id+'" class="prettyphoto" data-rel="'+rel+'">' +
        '<img src="'+image.src+'">' +
        '</a>';

      image.onload = function () {
        $(galleryContainer).empty().append(imageLink);
        // need to re-initialize rpetyphoto
        // on element added to DOM
        $('#currentGalleryImage').prettyPhoto();
      };




    },

    // get the first item from gallery slider
    // and load it to image container
    loadFirstImage : function() {

      var firstImage = $(imageCarousel).find('.vc_item').first(galleryItem).find(imageLoad);
      if (firstImage.length > 0) {
        ajax.loadGalleryItem(firstImage);
      }

    },

    hookHandlers: function () {

      $(imageLoad).off('click').on('click', function (event) {

        // Avoid following the href location when clicking
        event.preventDefault();
        event.stopPropagation();
        // load item
        ajax.loadGalleryItem($(this));
      });

    }


  }

  ajax.hookHandlers();
  ajax.loadFirstImage();


}