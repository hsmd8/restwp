/**
 * Navigation handler
 * @param $
 * @param navbar
 * @constructor
 */
function Navigation($, navBar, navBarAppearance) {
    "use strict";

    var menu = {

        hookHandlers : function() {

          $('.toggleDropdown').on('click', function(event) {
            var parent = $(this).parent();
            // Avoid following the href location when clicking
            event.preventDefault();
            // Avoid having the menu to close when clicking
            event.stopPropagation();
            // If a menu is already open we close it, else we open
            ($(parent).hasClass('open'))?($(parent).removeClass('open')):($(parent).addClass('open'));
          });

          $(window).scroll(function() {
            var height = $(window).scrollTop();

            if(height  >= 60) {
              $(navBar).css('top','0px').css('background', navBarAppearance.navBarColor);;
            } else {
              $(navBar).css('top','60px').css('background', navBarAppearance.navBarRgba);
            }
          });
        }
    }

  menu.hookHandlers();

}
