function Languages($, langBar) {


  var lang = {

    state : {
      isActive : false,
      isMobile : false,
      langsByCode : {},
      langsByLabel : {}
    },


    hookHandlers : function() {

      lang.fetchInitialData();

      if (lang.state.isActive) {
        lang.switchDisplay();

        $(window).on('resize', function () {
          lang.switchDisplay();
        });
      }

    },

    fetchInitialData : function() {

      var languages = $(langBar).find('a');
      if (!languages.length) {
        return;
      }
      lang.state.isActive = true;

      for (var i=0; i < languages.length; i++) {

        var thisLang = {};
        thisLang.code = languages[i].hreflang.toUpperCase();
        thisLang.label = languages[i].innerText;

        lang.state.langsByCode[thisLang.code] = thisLang;
        lang.state.langsByLabel[thisLang.label] = thisLang;

      }



    },

    switchDisplay : function() {

      if (lang.viewport().width <= 991) {
        if (!lang.state.isMobile) {
          lang.switchToMobile();
          lang.state.isMobile = true;
        }
      }
      else {
        if (lang.state.isMobile) {
          lang.switchToDesktop();
          lang.state.isMobile = false;
        }
      }

    },

    switchToMobile : function() {

      $(langBar).find('option').each(function(){

        var me = lang.state.langsByLabel[($(this).html())];
        $(this).html(me.code);

      });

    },

    switchToDesktop : function() {

      $(langBar).find('option').each(function(){

        var me = lang.state.langsByCode[($(this).html())];
        $(this).html(me.label);

      });

    },

    viewport : function() {
      var e = window, a = 'inner';
      if (!('innerWidth' in window )) {
        a = 'client';
        e = document.documentElement || document.body;
      }
      return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
    }
  }

  lang.hookHandlers();

}