/**
 * Theme initialization
 * @param $
 * @returns {{init: Function}}
 * @constructor
 */
var THEME = function ($) {
  "use strict";

  var navigation,
    ajaxGallery,
    languages,
    map;


  var init = function (input) {

    // Assign values according to the UI
    var UI = {

      navBar : '.navbar',
      galleryContainer : '#gallery-container',
      imageLoad : '.load-gallery-item',
      imageCarousel : '.vc_images_carousel',
      preloader : '.preloader',
      galleryItem : '.vc_item',
      mapContainerID : 'map',
      langBar : '.topBar__lang',
      mapAppearance : input.mapAppearance,
      navBarAppearance : input.navBarAppearance

    }

    // Call components
    navigation = Navigation($, UI.navBar, UI.navBarAppearance);
    ajaxGallery = AjaxGallery($, UI.galleryContainer, UI.imageLoad, UI.imageCarousel, UI.galleryItem);
    languages = Languages($, UI.langBar);
    map = Map(UI.mapAppearance, UI.mapContainerID);

    new WOW().init();

    $(document).on('preloader:finished', function () {
      $(UI.preloader).fadeOut();
    });


    if ($('.flexslider').length) {
      $('.flexslider').flexslider({
        animation: 'slide',
        slideshow: true,
        pauseOnHover: true,
        smoothHeight: false,
        prevText: "",
        nextText: ""
      }); //end flexslider
    }


  }

  return {
    init: init
  };

};
