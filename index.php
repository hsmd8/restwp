<?php get_header(); ?>

<?php
$additional_spacing = 90;
if (is_front_page()) {
    get_template_part('slider');
    if ($rest_options['slider-enabled'] == true) {
        $slider_id = esc_attr($rest_options['slider']);
        if ($slider_id == 'None') {
            $additional_spacing = 200;
        }
    } else{
        $additional_spacing = 200;
    }
}
?>
<?php
if (!(is_front_page())) { ?>

    <div class="page-header padding-top-200 padding-bottom-60">

        <div class="header-overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-8">
                        <div class="header-overlay__bg"></div>
                    </div>
                </div>
            </div>
        </div>


        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <h1><?php echo get_bloginfo('name');?></h1>
                </div>
            </div>
        </div>
    </div>

<?php } ?>

<div class="page-content padding-top-<?php echo $additional_spacing; ?> padding-bottom-50">
    <div class="container">
        <div class="col-md-12 clearfix">
            <?php query_posts("post_type=post&paged=" . get_query_var('paged'));
            if(have_posts()): while(have_posts()) : the_post();
                $post_name = $post->post_name;
                $post_id = get_the_ID();
                $content = get_the_content();
                ?>
                <div class="blog-content">
                    <div class="post padding-bottom-40 margin-bottom-40">
                        <?php if ( has_post_thumbnail() ) {
                            ?>
                            <div class="post-feature padding-bottom-20">
                                <a data-rel="prettyPhoto" href="<?php echo wp_get_attachment_url(get_post_thumbnail_id($post -> ID)); ?>" title="<?php esc_attr(the_title()); ?>">
                                    <?php the_post_thumbnail('full', array( 'class' => "imagepost wow fadeIn")); ?>
                                </a>
                            </div>
                        <?php }?>
                        <div class="row">
                            <div class="col-md-2 col-sm-2 col-xs-2 align-center post-date">
                                <i class="fa fa-camera"></i>
                                <h6 class="month"><?php echo get_the_date("M");?></h6>
                                <h1 class="day"><?php echo get_the_date("d");?></h1>
                            </div>
                            <div class="col-md-10 col-sm-10 col-xs-10">
                                <h2><?php echo get_the_title();?></h2>

                                <div class="post-info padding-bottom-20 padding-top-20">
                                    <i class="fa fa-pencil-square"></i>
                                    <span><?php echo '' . __('autor: ', 'rest') . ' ' . get_the_author_link(); ?></span>
                                    <i class="fa fa-calendar"></i>
                                    <span><?php echo '' . __('dnia:  ', 'rest') . ' ' . get_the_date(); ?></span>
                                </div>
                                <div class="post-content padding-bottom-30">
                                    <?php echo wp_trim_words( $content , 100 ); ?>
                                </div>
                                <div class="read-more padding-top-10 align-right">
                                    <a href="<?php the_permalink(); ?>" role="button" class="btn btn-lg btn-dark"><?php echo __('WIĘCEJ', 'rest'); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            endwhile; else: ?>
                <p><?php _e('Nie odnaleziono postów spełniających zadane kryteria.', 'rest'); ?></p>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>