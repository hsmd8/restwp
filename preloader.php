<?php
global $rest_options;
if ((array_key_exists('preloader-enabled', $rest_options) && $rest_options['preloader-enabled'] == 1 )) {
    if ((array_key_exists('preloader-pages', $rest_options) && $rest_options['preloader-pages'] === 'main' )) {
        if (is_front_page()) {?>
            <div class="preloader"></div>
        <?php } } else { ?>
        <div class="preloader"></div>
    <?php }
}
?>