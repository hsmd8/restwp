<?php wp_footer(); ?>
<?php global $rest_options;

$footer_class = "";
if ( is_admin_bar_showing() ) {
    $footer_class = "admin-bar-showing";
}

?>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="footer__widget col-md-2 align-center">
                <?php
                if ((array_key_exists('footer-logo', $rest_options) && $rest_options['footer-logo'] !== "")) {
                    echo '<a href="' . esc_url(home_url()) . '">' .
                        wp_get_attachment_image($rest_options['footer-logo']['id'], 'full') .
                        '</a>';
                }
                ?>
            </div>
            <div class="footer__widget col-md-6 align-center">
                <div class="padding-top-20">
                    <?php
                    echo '<a href="' . esc_url(home_url()) . '">';
                    bloginfo('name');
                    echo '</a> ';
                    if ((array_key_exists('footer_adres', $rest_options) && $rest_options['footer_adres'] !== "")) {
                        echo $rest_options['footer_adres'];
                    }
                    if ((array_key_exists('footer_telefon', $rest_options) && $rest_options['footer_telefon'] !== "")) {
                        echo ' <a href="' . $rest_options['footer_telefon_link'] . '">' . $rest_options['footer_telefon'] . '</a>';
                    }
                    ?>
                </div>
                <?php echo $rest_options['lower_footer'];?>
            </div>
            <div class="footer__widget col-md-3 align-center">
                <?php
                if (array_key_exists('social_footer', $rest_options)) {
                    foreach ($rest_options['social_footer'] as &$value) {
                        if ((array_key_exists($value, $rest_options)) && $rest_options[$value] !== "") {
                            echo '<a href="' . $rest_options[$value] . '" class="footer__icon"><i class="fa ' . $value . '"></i></a>';
                        }
                    }
                }
                ?>
            </div>
        </div>
    </div>
</footer>


<script>


    (function ($) {
        "use strict";

        $(document).ready(function () {
            <?php
            if ((array_key_exists('preloader-enabled', $rest_options) && $rest_options['preloader-enabled'] == 1 )) {
                if ((array_key_exists('preloader-pages', $rest_options) && $rest_options['preloader-pages'] === 'main' )) {
                    if (is_front_page()) {?>
            $('body').jpreLoader();
            <?php } } else { ?>
            $('body').jpreLoader();
            <?php }
         }
         ?>


            <?php
            $popup = str_replace(array("\r", "\n"), '', $rest_options["map-popup"]);
            ?>
            THEME($).init({

                mapAppearance: {
                    mapZoom: <?php echo $rest_options["map-zoom"];?>,
                    mapMarker: '<?php echo $rest_options["map-marker"]["url"];?>',
                    mapLat: <?php echo $rest_options["map-lat"];?>,
                    mapLng: <?php echo $rest_options["map-lng"];?>,
                    mapTitle: '<?php echo $rest_options["map-title"];?>',
                    mapPopup: '<?php echo $popup;?>'
                },

                navBarAppearance: {
                    navBarRgba:  '<?php echo $rest_options["navbar-background"]["rgba"];?>',
                    navBarColor: '<?php echo $rest_options["navbar-background"]["color"];?>'
                }



            });
        });

    })(jQuery);

    <?php
         if (array_key_exists('include-js', $rest_options) && $rest_options['include-js'] == 1) {
             echo $rest_options['custom-js'];
         }
    ?>

</script>
</body>
</html>