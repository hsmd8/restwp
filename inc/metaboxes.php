<?php

if(function_exists("register_field_group"))
{
    register_field_group(array (
        'id' => 'acf_ustawienia-strony',
        'title' => '[:pl]Ustawienia strony[:]',
        'fields' => array (
            array (
                'key' => 'field_594a4b75c36ba',
                'label' => 'Kolor Tła Strony',
                'name' => 'page_bg_color',
                'type' => 'color_picker',
                'instructions' => 'Wybierz kolor tła strony',
                'default_value' => '',
            ),
            array (
                'key' => 'field_594a4cd9fba88',
                'label' => 'Rodzaj nagłówka',
                'name' => 'page_header_type',
                'type' => 'radio',
                'required' => 1,
                'choices' => array (
                    'page_header_bg_color' => 'Jednolity kolor tła',
                    'page_header_bg_image' => 'Obrazek w tle',
                ),
                'other_choice' => 0,
                'save_other_choice' => 0,
                'default_value' => 'page_header_bg_color',
                'layout' => 'vertical',
            ),
            array (
                'key' => 'field_594a4d52b6141',
                'label' => 'Kolor tła nagłówka',
                'name' => 'page_header_bg_color_value',
                'type' => 'color_picker',
                'conditional_logic' => array (
                    'status' => 1,
                    'rules' => array (
                        array (
                            'field' => 'field_594a4cd9fba88',
                            'operator' => '==',
                            'value' => 'page_header_bg_color',
                        ),
                    ),
                    'allorany' => 'all',
                ),
                'default_value' => '',
            ),
            array (
                'key' => 'field_594a4d9ab3e2f',
                'label' => 'Obrazek tła nagłówka',
                'name' => 'page_header_bg_image_value',
                'type' => 'image',
                'conditional_logic' => array (
                    'status' => 1,
                    'rules' => array (
                        array (
                            'field' => 'field_594a4cd9fba88',
                            'operator' => '==',
                            'value' => 'page_header_bg_image',
                        ),
                    ),
                    'allorany' => 'all',
                ),
                'save_format' => 'url',
                'preview_size' => 'medium',
                'library' => 'all',
            ),
            array (
                'key' => 'field_594a4def8da0f',
                'label' => 'Kolor tekstu nagłówka',
                'name' => 'page_header_font_color',
                'type' => 'color_picker',
                'default_value' => '#000',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'page',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post',
                    'order_no' => 0,
                    'group_no' => 1,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
                0 => 'discussion',
                1 => 'comments',
                2 => 'revisions',
                3 => 'categories',
                4 => 'tags',
            ),
        ),
        'menu_order' => 0,
    ));
}