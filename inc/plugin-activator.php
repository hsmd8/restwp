<?php

require_once get_template_directory() . '/inc/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'rest_register_required_plugins' );

/**
 * Register the required plugins for this theme.
 *
 * In this example, we register five plugins:
 * - one included with the TGMPA library
 * - two from an external source, one from an arbitrary source, one from a GitHub repository
 * - two from the .org repo, where one demonstrates the use of the `is_callable` argument
 *
 * The variables passed to the `tgmpa()` function should be:
 * - an array of plugin arrays;
 * - optionally a configuration array.
 * If you are not changing anything in the configuration array, you can remove the array and remove the
 * variable from the function call: `tgmpa( $plugins );`.
 * In that case, the TGMPA default settings will be used.
 *
 * This function is hooked into `tgmpa_register`, which is fired on the WP `init` action on priority 10.
 */
function rest_register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(

		// This is an example of how to include a plugin bundled with a theme.
		array(
			'name'               => 'Rest Plugin', // The plugin name.
			'slug'               => 'rest', // The plugin slug (typically the folder name).
			'source'             => get_template_directory() . '/inc/plugins/rest.zip', // The plugin source.
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			'force_activation'   => true, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		),

        array(
            'name'               => 'Revolution Slider', // The plugin name.
            'slug'               => 'revslider', // The plugin slug (typically the folder name).
            'source'             => get_template_directory() . '/inc/plugins/revslider.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
            'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
            'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
            'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
            'external_url'       => '', // If set, overrides default API URL and points to an external URL.
            'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
        ),

        array(
            'name'               => 'Visual Composer', // The plugin name.
            'slug'               => 'js_composer', // The plugin slug (typically the folder name).
            'source'             => get_template_directory() . '/inc/plugins/js_composer.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
            'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
            'force_activation'   => true, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
            'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
            'external_url'       => '', // If set, overrides default API URL and points to an external URL.
            'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
        ),


		// This is an example of how to include a plugin from the WordPress Plugin Repository.
		array(
			'name'      => 'Contact Form 7',
			'slug'      => 'contact-form-7',
			'required'  => false,
		),

        array(
            'name'              => 'Advanced Custom Fields',
            'slug'              => 'advanced-custom-fields',
            'force_activation'   => true,
            'required'          => true,
        ),


        array(
            'name'              => 'Redux Framework',
            'slug'              => 'redux-framework',
            'force_activation'   => true,
            'required'          => true,
        ),


	);

	/*
	 * Array of configuration settings. Amend each line as needed.
	 *
	 * TGMPA will start providing localized text strings soon. If you already have translations of our standard
	 * strings available, please help us make TGMPA even better by giving us access to these translations or by
	 * sending in a pull-request with .po file(s) with the translations.
	 *
	 * Only uncomment the strings in the config array if you want to customize the strings.
	 */
	$config = array(
		'id'           => 'rest',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.


		'strings'      => array(
			'page_title'                      => 'Zainstaluj wymagane wtyczki',
			'menu_title'                      => 'Zainstaluj wtyczki',

			'installing'                      => 'Instalowanie',

			'updating'                        => 'Aktualizowanie',
			'oops'                            => 'Coś poszło nie tak',
			'notice_can_install_required'     => _n_noop(

				'Skórka wymaga następujących wtyczek: %1$s.',
				'Skórka wymaga następujących wtyczek: %1$s.',
				'notextdomain'
			),
			'notice_can_install_recommended'  => _n_noop(

				'Skórka rekomenduje następujące wtyczki: %1$s.',
				'Skórka rekomenduje następujące wtyczki: %1$s.',
				'notextdomain'
			),
			'notice_ask_to_update'            => _n_noop(

				'Te wtyczki wymagają zaktualizowania: %1$s.',
				'Te wtyczki wymagają zaktualizowania: %1$s.',
				'notextdomain'
			),
			'notice_ask_to_update_maybe'      => _n_noop(

				'Dostępna jest aktualizacja dla: %1$s.',
				'Dostępna jest aktualizacja dla: %1$s.',
				'notextdomain'
			),
			'notice_can_activate_required'    => _n_noop(
				'Wymagane wtyczki nie zostały aktywowane: %1$s.',
				'Wymagane wtyczki nie zostały aktywowane: %1$s.',
				'notextdomain'
			),
			'notice_can_activate_recommended' => _n_noop(

				'Rekomendowane wtyczki nie zostały aktywowane: %1$s.',
				'Rekomendowane wtyczki nie zostały aktywowane: %1$s.',
				'notextdomain'
			),
			'install_link'                    => _n_noop(
				'Rozpocznij instalację wtyczek',
				'Rozpocznij instalację wtyczek',
				'notextdomain'
			),
			'update_link' 					  => _n_noop(
				'Rozpocznij aktualizację wtyczek',
				'Rozpocznij aktualizację wtyczek',
				'notextdomain'
			),
			'activate_link'                   => _n_noop(
				'Rozpocznij aktywację wtyczek',
				'Rozpocznij aktywację wtyczek',
				'notextdomain'
			),
			'return'                          => 'Powrót do installera wtyczek',
			'plugin_activated'                => 'Wtyczka została aktywowana.',
			'activated_successfully'          => 'Wtyczka została aktywowana:',

			'plugin_already_active'           => 'Wtyczka %1$s była już aktywna.',

			'plugin_needs_higher_version'     => 'Nie aktywowano wtyczki - wymagana jest wyższa wersja.',

			'complete'                        => 'Wszystkie wtyczki pomyślnie zainstalowane i aktywowane. %1$s',
			'dismiss'                         => 'Przeczytałem',
			'notice_cannot_install_activate'  => 'Pozostały wtyczki do zainstalowania lub aktywowania.',
			'contact_admin'                   => 'Skontaktuj się z administratorem.',

			'nag_type'                        => '', // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
		),

	);

	tgmpa( $plugins, $config );
}
