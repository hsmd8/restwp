<?php get_header(); ?>


    <div class="page-header padding-top-150 error-page padding-bottom-80">
        <div class="container">
            <div class="row">
                <div class="col-md-12 align-center error padding-top-100">
                    <div class="error-shadow wow fadeIn">
                        <h1 class="padding-bottom-60"> <?php _e('404', 'rest'); ?> </h1>
                    </div>
                    <h1 class="padding-bottom-80"><?php _e('Ups... ', 'rest'); ?>
                        <span class="medium-thin-font"><?php _e('coś', 'rest'); ?></span>
                        <?php _e('poszło nie tak', 'rest'); ?>
                    </h1>

                    <h4><?php _e('Strona której szukasz nie została odnaleniona', 'rest'); ?></h4>

                    <div class="return-home wow fadeIn padding-bottom-80 padding-top-80">
                        <a href="<?php echo esc_url(home_url()); ?>"><i class="fa fa-home"></i></a>
                    </div>

                    <p>
                        <?php _e('Sprawdź czy adres nie zawiera błędów,', 'rest'); ?>
                        <BR>
                        <?php _e('lub kliknij w powyższy przycisk, aby powrócić na ', 'rest'); ?>
                        <strong><?php _e('stronę główną.', 'rest'); ?></strong>
                    </p>
                    <?php wp_link_pages(); ?>
                </div>
            </div>
        </div>
    </div>



<?php get_footer(); ?>