<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php
    global $rest_options;

    if (array_key_exists('favicon', $rest_options)) {
          echo '<link rel="shortcut icon" href="' . esc_url($rest_options['favicon']['url']) . '"/>';
    }
    ?>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php
    if (array_key_exists('include-css', $rest_options) && $rest_options['include-css'] == 1) {
        echo '<style type="text/css">';
        echo $rest_options['custom-css'];
        echo '</style>';
    }
    ?>

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php
get_template_part('preloader');
get_template_part('navigation');
?>