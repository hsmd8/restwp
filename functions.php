<?php


function rest_theme_setup() {
    load_theme_textdomain( 'rest', get_template_directory() . '/lang' );

    $locale = get_locale();
    $locale_file = get_template_directory() . "/lang/$locale.php";

    if ( is_readable( $locale_file ) ) {
        require_once( $locale_file );
    }
}
add_action( 'after_setup_theme', 'rest_theme_setup' );



// include metaboxes settings
define( 'ACF_LITE', true );
include_once (get_template_directory() . '/inc/metaboxes.php');


// include admin settings
require_once (dirname(__FILE__) . '/admin/admin.php');

// include plugin activator
include_once (get_template_directory() . '/inc/plugin-activator.php');


// add theme support for title tag
add_theme_support( 'title-tag' );

// add theme support for feed links
add_theme_support( 'automatic-feed-links' );


// set content width
if ( ! isset( $content_width ) ) {
    $content_width = 1200;
}

// Flush rewrite rules
function rest_flush_rewrite_rules() {
    flush_rewrite_rules();
}
add_action('after_switch_theme', 'rest_flush_rewrite_rules');


// include scripts for frontend
function theme_enqueue_scripts() {
    global $rest_options;
    if(!is_admin()) {
        wp_enqueue_script('theme-scripts', get_template_directory_uri() . '/build/js/scripts.min.js', array('jquery'), '', false);
        if ((array_key_exists('map-api', $rest_options)) && $rest_options['map-api'] !== "") {
            wp_enqueue_script('googlemap', 'https://maps.googleapis.com/maps/api/js?key=' . $rest_options['map-api'], array(), '', false);

        }

    }
}
add_action('wp_enqueue_scripts', 'theme_enqueue_scripts');



function remove_menus(){

    remove_menu_page( 'edit-comments.php' );          //Comments

}
add_action( 'admin_menu', 'remove_menus' );

// include theme styles
// if a new stylesheet has been created by Admin panel, attach it
// else attach default file
function theme_enqueue_styles() {

    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/rest/theme.css';

    if(!is_admin()) {
        wp_enqueue_style('theme-scripts', get_template_directory_uri() . '/build/css/style.min.css');

        if (class_exists( 'Redux' ) && class_exists( 'Rest' ) && file_exists($upload_dir)){
            wp_enqueue_style( 'theme', $upload['baseurl'] . '/rest/theme.css');
        } else {
            wp_enqueue_style('theme-main', get_template_directory_uri() . '/build/css/theme.min.css');
        }
    }


}
add_action('wp_enqueue_scripts', 'theme_enqueue_styles');


// load google fonts
function rest_add_google_fonts() {
    wp_register_style('Playfair', 'https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i');
    wp_enqueue_style('Playfair');
    wp_register_style('Lato', 'https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i');
    wp_enqueue_style('Lato');
}
add_action('wp_enqueue_scripts', 'rest_add_google_fonts');



// register menu
function rest_register_menus() {
    register_nav_menus(array(
        'primary-menu' => 'Primary Navigation Menu',
        'restaurant-menu' => 'Restaurant Navigation Menu'
    ));
}
add_action('init', 'rest_register_menus');



// thumbnails sizes
set_post_thumbnail_size(900, 600, false);
add_image_size('gallery-thumb', 500, 300, true);
add_image_size('gallery-image', 847.5, 596, true);
add_image_size('avatars', 100, 100, true);


function remove_admin_login_header() {
    remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('get_header', 'remove_admin_login_header');


// page sidebar
register_sidebar(array('name' => __('Widgety', 'rest'), 'id' => 'page-sidebar', 'description' => __('Widgety.', 'hypno'), 'before_widget' => '<div id="%1$s" class="widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h3>', 'after_title' => '</h3>'));




/**
 * Class Rest_Menu_Walker
 * Custom navigation markup
 */

class Rest_Menu_Walker extends Walker_Nav_Menu {

    function start_lvl(&$output, $depth = 0, $args = array()) {
        $indent = str_repeat("\t", $depth);
        // Change sub-menu to dropdown menu
        $output .= "\n$indent<ul class=\"dropdown-menu span10\">\n";
    }

    function start_el ( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        global $wp_query, $wpdb;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $class_names = $value = '';
        $postmeta = $wpdb->prefix . 'postmeta';
        $has_children = $wpdb->get_var("SELECT COUNT(meta_id)
                            FROM $postmeta
                            WHERE meta_key='_menu_item_menu_item_parent'
                            AND meta_value='".$item->ID."'");

        $dropdown = "";

        // add dropdown class
        if ($has_children) {
            $dropdown = "dropdown";
        }

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $classes[] = 'menu-item-' . $item->ID;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
        $class_names = ' class="' . esc_attr( $class_names ) . ' ' . $dropdown .'"';

        $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
        $id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';


        $output .= $indent . '<li' . $id . $value . $class_names .'>';

        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

        // Check if menu item is in main menu
        if ( $has_children > 0  ) {
            // These lines adds your custom class and attribute
            $attributes .= ' class="dropdown-toggle"';
        }

        $item_output = $args->before;

        // Add the caret if menu level is 0
        if ( $has_children > 0  ) {
            $item_output .= '<span class="toggleDropdown" href="#" data-toggle="dropdown"><i class="fa fa-angle-double-down"></i></span>';
        }

        $item_output .= '<a'. $attributes .'>';
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;

        $item_output .= '</a>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }

}
