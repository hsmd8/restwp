<?php get_header();
if (is_front_page()) { get_template_part('slider'); }


if (have_posts()) : while (have_posts()) : the_post();

    $title = get_the_title();

    $header_font_color = $header_bg_color = $header_bg_image = $bg_color = '';

    $meta_bg_color = get_field('page_bg_color');
    $meta_header_type = get_field('page_header_type');
    $meta_header_bg_color = get_field('page_header_bg_color_value');
    $meta_header_bg_image = get_field('page_header_bg_image_value');
    $meta_header_font_color = get_field('page_header_font_color');

    if ($meta_header_font_color !== '') {
        $header_font_color = 'style="color: ' . $meta_header_font_color . '"';
    }

    if ($meta_bg_color !== '') {
        $bg_color = 'style="background-color: ' . $meta_bg_color . '"';
    }

    if ('page_header_bg_color' === $meta_header_type && $meta_header_bg_color !== '') {
        $header_bg_color = 'style="background-color: ' . $meta_header_bg_color . '"';
    } else if ($meta_header_bg_image !== false) {
        $header_bg_image = 'style="background-image: url(' . $meta_header_bg_image . ')"';
    }




?>

    <?php if (!is_front_page()) { ?>
    <div class="page-header padding-top-200 padding-bottom-60" <?php echo $header_bg_color . $header_bg_image;?> >

        <div class="header-overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-8">
                        <div class="header-overlay__bg"></div>
                    </div>
                </div>
            </div>
        </div>


        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-8 align-center wow fadeIn">
                    <h1 <?php echo $header_font_color;?>><?php echo $title;?></h1>
                </div>
            </div>
        </div>
    </div>
    <?php }?>

    <div class="page-content" <?php echo $bg_color;?>>
        <div class="container">
            <div class="row">
                <div class="col-md-12 clearfix">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>


<?php
endwhile;
endif;
?>
<?php get_footer(); ?>