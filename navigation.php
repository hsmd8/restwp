<!--navigation start-->
<?php
    global $rest_options;
?>

<div class="topBar">
    <div class="container">
        <div class="row">
            <div class="col-md-8 hidden-sm hidden-xs">
                <?php if ((array_key_exists('show_email', $rest_options) && $rest_options['show_email'] == true)) { ?>
                    <div class="topBar__icon">
                        <i class="fa fa-envelope-o"></i> <?php echo $rest_options['email']; ?>
                    </div>
                <?php } ?>
                <?php if ((array_key_exists('show_phone', $rest_options) && $rest_options['show_phone'] == true)) { ?>
                    <div class="topBar__icon">
                        <i class="fa fa-phone"></i> <?php echo $rest_options['phone']; ?>
                    </div>
                <?php } ?>
                <?php if ((array_key_exists('show_info', $rest_options) && $rest_options['show_info'] == true)) { ?>
                    <div class="topBar__icon topBar__icon--hide">
                        <i class="fa fa-clock-o"></i> <?php echo $rest_options['recepcja']; ?>
                    </div>
                <?php } ?>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-7 topBar__lang align-center medium-align-right">
                        <i class="fa fa-globe"></i>
                        <?php echo the_widget('qTranslateXWidget', array('type' => 'dropdown', 'hide-title' => true, 'widget-css-off' => true) );?>
                    </div>
                    <div class="col-md-5 hidden-sm hidden-xs topBar__social align-center medium-align-right">
                        <?php
                        if (array_key_exists('social-header', $rest_options)) {
                            foreach ($rest_options['social-header'] as &$value) {
                                if ((array_key_exists($value, $rest_options)) && $rest_options[$value] !== "") {
                                    echo '<a href="' . $rest_options[$value] . '"><i class="fa ' . $value . '"></i></a>';
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<nav class="navbar navbar-fixed-top sticky">

    <div id="navbar">

        <div class="container">
            <div class="row">

                <div class="hidden-lg hidden-md hidden-sm col-xs-12 mobile-section">
                    <div class="mobile-logo">
                        <?php
                        if ((array_key_exists('mobile-logo', $rest_options) && $rest_options['mobile-logo'] !== "")) {
                            echo '<a href="' . esc_url(home_url()) . '">' .
                                wp_get_attachment_image($rest_options['mobile-logo']['id'], 'full') .
                                '</a>';
                        }
                        ?>
                    </div>
                </div>

                <div class="col-md-12 col-sm-6 col-xs-12">


            <div class="row">
                <div class="col-md-8 align-center">
                    <?php
                    if (has_nav_menu( 'primary-menu' )) {
                        wp_nav_menu(array('container' => '', 'menu_id' => 'primary-menu', 'menu_class' => 'nav navbar-nav', 'theme_location' => 'primary-menu', 'depth' => 2, 'walker' => new Rest_Menu_Walker()));
                    } else {
                        // if no primary menu is configured
                        echo '<ul class="nav navbar-nav"><li><a href="'.esc_url(home_url()).'/wp-admin/nav-menus.php">Skonfiguruj Menu</a></li></ul>';
                    }
                    ?>
                </div>
                <div class="col-md-4 align-center">
                    <?php
                    if (has_nav_menu( 'restaurant-menu' )) {
                        wp_nav_menu(array('container' => '', 'menu_id' => 'restaurant-menu', 'menu_class' => 'nav navbar-nav', 'theme_location' => 'restaurant-menu', 'depth' => 1));
                    }
                    ?>
                </div>
            </div>

                </div>
                <div class="hidden-lg hidden-md col-sm-6 col-xs-12 mobile-section">

                    <div class="mobile-logo hidden-xs">
                        <?php
                        if ((array_key_exists('mobile-logo', $rest_options) && $rest_options['mobile-logo'] !== "")) {
                            echo '<a href="' . esc_url(home_url()) . '">' .
                                wp_get_attachment_image($rest_options['mobile-logo']['id'], 'full') .
                                '</a>';
                        }
                        ?>
                    </div>

                    <div class="mobile-info">

                        <?php
                        if ((array_key_exists('footer_adres', $rest_options) && $rest_options['footer_adres'] !== "")) {
                            echo '<span class=""><i class="fa fa-envelope"></i> '. $rest_options['footer_adres'] . '</span>';
                        }
                        if ((array_key_exists('footer_telefon', $rest_options) && $rest_options['footer_telefon'] !== "")) {
                            echo '<span class=""><i class="fa fa-phone"></i> <a href="' . $rest_options['footer_telefon_link'] . '">' . $rest_options['footer_telefon'] . '</a></span>';
                        }
                        ?>

                        <div class="mobile-sticker">
                            <?php
                            if ((array_key_exists('mobile-sticker', $rest_options) && $rest_options['mobile-sticker'] !== "")) {
                                echo wp_get_attachment_image($rest_options['mobile-sticker']['id'], 'full');
                            }
                            ?>
                            <div class="mobile-sticker__overlay">
                                <?php
                                if ((array_key_exists('mobile-sticker-text', $rest_options) && $rest_options['mobile-sticker-text'] !== "")) {
                                    echo $rest_options['mobile-sticker-text'];
                                }
                                ?>
                            </div>
                        </div>

                        <?php

                        if ((array_key_exists('mobile-map-link', $rest_options) && $rest_options['mobile-map-link'] !== "")) {
                            echo '<a href="'.$rest_options['mobile-map-link'].'" role="button" class="btn btn--transparent btn--accent-label btn-md wow fadeIn">'.$rest_options['mobile-map-text'].'</a>';
                        }

                        ?>


                    </div>


                </div>


            </div>
        </div>

    </div>

</nav>
<!-- navigaqtion end-->